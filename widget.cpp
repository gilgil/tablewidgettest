#include "widget.h"
#include "ui_widget.h"
#include <QScroller>
#include <QScrollerProperties>

Widget::Widget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Widget)
{
	ui->setupUi(this);
	QTableWidget* tableWidget = ui->tableWidget;
	int numColumns = 3;
	int numRows= 100;
	tableWidget->clear();
	tableWidget->setRowCount(numRows);
	tableWidget->setColumnCount(numColumns);
	for (auto r=0; r<numRows; r++)
		 for (auto c=0; c<numColumns; c++)
			  tableWidget->setItem( r, c, new QTableWidgetItem(QString::number(c) + QString::number(r)));


	tableWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	QScrollerProperties sp;
	sp.setScrollMetric(QScrollerProperties::DragVelocitySmoothingFactor, 0.6);
	sp.setScrollMetric(QScrollerProperties::MinimumVelocity, 0.0);
	sp.setScrollMetric(QScrollerProperties::MaximumVelocity, 0.5);
	sp.setScrollMetric(QScrollerProperties::AcceleratingFlickMaximumTime, 0.4);
	sp.setScrollMetric(QScrollerProperties::AcceleratingFlickSpeedupFactor, 1.2);
	sp.setScrollMetric(QScrollerProperties::SnapPositionRatio, 0.2);
	sp.setScrollMetric(QScrollerProperties::MaximumClickThroughVelocity, 0);
	sp.setScrollMetric(QScrollerProperties::DragStartDistance, 0.001);
	sp.setScrollMetric(QScrollerProperties::MousePressEventDelay, 0.5);
	QScroller* scroller = QScroller::scroller(tableWidget);
	scroller->grabGesture(tableWidget, QScroller::LeftMouseButtonGesture);
	scroller->setScrollerProperties(sp);
}

Widget::~Widget()
{
	delete ui;
}

